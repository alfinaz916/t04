class readystock extends kue{

    private double jumlah;

    public readystock(String name, double price, int jumlah) {
        super(name, price);
        this.jumlah = jumlah;
    }

    public double Berat() {
        return 0;
    }

    public double Jumlah() {
        return jumlah;
    }

    public double hitungHarga() {
        return getPrice()*jumlah*2;
    }
    
}