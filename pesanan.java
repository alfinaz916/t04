class pesanan extends kue{
    
    private double berat;

    public pesanan(String name, double price, int berat) {
        super(name, price);
        this.berat = berat;
    }

    public double Berat() {
        return berat;
    }

    public double Jumlah() {
        return 0;
    }

    public double hitungHarga() {
        return getPrice() * berat;
    }

    
}